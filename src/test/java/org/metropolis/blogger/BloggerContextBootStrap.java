package org.metropolis.blogger;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by metropolis on 2/4/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration()
public class BloggerContextBootStrap {
}
