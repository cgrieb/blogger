package org.metropolis.blogger.service;

import org.junit.Assert;
import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by metropolis on 2/9/2017.
 */
public class PasswordEncodeServiceTest extends BloggerContextBootStrap {
    @Autowired
    private PasswordEncodeService passwordEncodeService;

    private static final String STR_TO_ENCODE = "password";
    private static final String ENCODED_STR = "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8";

    @Test
    public void encodeUserPassword() {
        Assert.assertEquals(passwordEncodeService.passwordEncode(STR_TO_ENCODE), ENCODED_STR);

    }
}
