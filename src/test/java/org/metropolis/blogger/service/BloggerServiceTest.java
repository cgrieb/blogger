package org.metropolis.blogger.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.metropolis.blogger.entity.BlogComment;
import org.metropolis.blogger.entity.BlogData;
import org.metropolis.blogger.entity.BlogItem;
import org.metropolis.blogger.entity.BloggerUserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.List;
import java.util.Map;

/**
 * Created by metropolis on 2/6/2017.
 */
public class BloggerServiceTest extends BloggerContextBootStrap {
    @Autowired
    private BloggerService bloggerService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private static UserDetails userDetails;
    private static BlogItem blogToAdd;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_ROLE = "admin";

    @Before
    public void setup() {
        blogToAdd = new BlogItem();
        blogToAdd.setName("service-blog");
        blogToAdd.setTitle("Service Blog");
        blogToAdd.setDescription("This is a New Blog from the Service");
        blogToAdd.setCreated(1486356655193L);
        blogToAdd.setDeleted(true);
        blogToAdd.setSecured(false);
        blogToAdd.setOwner(ADMIN_USER);
        userDetails = this.setAuthentication();
    }

    @Test
    public void testGetAllSitesForUser() {
        List<BlogItem> blogItemList = bloggerService.getAllSitesForUser(userDetails);
        Assert.assertEquals(blogItemList.size(),3);
    }

    @Test
    public void testNewBlogCreation() {
       boolean results = bloggerService.createNewBlog(blogToAdd,userDetails);
       Assert.assertTrue(results);
    }

    @Test
    public void testCreationOfNewPosting() {
        Long results = bloggerService.createNewPosting("this-that", userDetails);
        Assert.assertNotNull(results);
    }

    @Test
    public void testSaveExistingBlog() {
        BlogItem blogToModify = bloggerService.getBlog("foobar");
        blogToModify.setDescription("Yep we've modified this blog");
        bloggerService.saveExistingBlog(blogToModify);
        blogToModify = bloggerService.getBlog("foobar");
        Assert.assertEquals(blogToModify.getDescription(), "Yep we've modified this blog");
        blogToModify.setDeleted(false);
        bloggerService.saveExistingBlog(blogToModify);
    }

    @Test
    public void testBlogExtraction() {
        BlogItem blogToExtract = bloggerService.getBlog("foobar");
        Assert.assertEquals(blogToExtract.getName(), "foobar");
        Assert.assertEquals(blogToExtract.getOwner(), "role.admin@email.com");
        Assert.assertEquals(blogToExtract.getTitle(), "FooBar");
        Assert.assertEquals(blogToExtract.isEnabled(),true);
        Assert.assertEquals(blogToExtract.isSecured(),false);
        Assert.assertEquals(blogToExtract.isDeleted(),false);
    }

    @Test
    public void testBlogTotal() {
        int results = bloggerService.getSiteTotal(userDetails);
        Assert.assertEquals(results,3);
    }

    @Test
    public void testGetDataByFirstBlock() {
        BlogData blogData = bloggerService.getDataByFirstDataBlock("hamminator");
        Assert.assertNotNull(blogData);
        Assert.assertEquals(blogData.getBlogName(),"hamminator");
    }

    @Test
    public void testDataSave() {
        BlogData blogData = bloggerService.getDataByFirstDataBlock("hamminator");
        blogData.setData("We have modified this data set.");
        bloggerService.saveBlogData(blogData);
        blogData = bloggerService.getDataByFirstDataBlock("hamminator");
        Assert.assertEquals(blogData.getData(), "We have modified this data set.");
    }

    @Test
    public void testCommentAdditionAndDeletion() {
        BlogComment blogComment = new BlogComment();
        blogComment.setBlogName("hamminator");
        blogComment.setBlogPosting(1486224744055L);
        blogComment.setComment("This is a blog comment for the blog hamminator");
        blogComment.setCommentOwner(ADMIN_USER);
        blogComment.setCommentId(3);
        blogComment.setCommentPosted(1486262513660L);
        bloggerService.addBlogComment(blogComment);
        bloggerService.deleteComment(blogComment);
        Assert.assertEquals(bloggerService.getCommentsForBlog("hamminator", 1486224744055L).size(),2);
    }

    @Test
    public void testBlogCommentExtraction() {
        Assert.assertEquals(bloggerService.getCommentsForBlog("hamminator", 1486224744055L).size(),2);
    }

    @Test
    public void performASearchForABlog() {
        List<BlogItem> blogItems = bloggerService.search("hamminator");
        Assert.assertEquals(blogItems.size(),1);
    }

    @Test
    public void testBlogAndBlogOwner() throws Exception {
        Map<String,Object> blogAndUserInformation = bloggerService.getBlogAndUserInfo("hamminator");
        blogAndUserInformation.forEach((k,v)->{
            if("blog".equals(k)) {
                BlogItem blogItem = (BlogItem)v;
                Assert.assertEquals(blogItem.getName(), "hamminator");
                Assert.assertEquals(blogItem.getOwner(), ADMIN_USER);
                Assert.assertEquals(blogItem.getTitle(),"Other Stuff");
                Assert.assertEquals(blogItem.getDescription(), "A blog about Stuff and Things");
                Assert.assertTrue(blogItem.isEnabled());
                Assert.assertFalse(blogItem.isSecured());
                Assert.assertFalse(blogItem.isDeleted());
            }

            if("user".equals(k)) {
                BloggerUserAccount bloggerUserAccount = (BloggerUserAccount)v;
                Assert.assertEquals(bloggerUserAccount.getUsername(),ADMIN_USER);
                Assert.assertEquals(bloggerUserAccount.getPassword(),ADMIN_PASSWORD);
                Assert.assertTrue(bloggerUserAccount.isEnabled());
                Assert.assertEquals(bloggerUserAccount.getFirstName(),"Admin");
                Assert.assertEquals(bloggerUserAccount.getLastName(),"User");
                Assert.assertEquals(bloggerUserAccount.getRole(),ADMIN_ROLE);

            }
        });
    }

    @Test
    public void testBlogDataAndComments() {
        Map<String,List> dataAndComments = bloggerService.getDataAndComments("hamminator",1486224744055L);
        dataAndComments.forEach((k,v)->{
            if("comments".equals(k)) {
                Assert.assertEquals(v.size(),2);
            }

            if("data".equals(k)) {
                Assert.assertEquals(v.size(),3);
            }
        });
    }

    @Test
    public void testBlogDeactivationAndReactivation() {
        bloggerService.deactivateBlog("hamminator");
        BlogItem blogItem = bloggerService.getBlog("hamminator");
        Assert.assertTrue(blogItem.isDeleted());
        blogItem.setDeleted(false);
        bloggerService.saveExistingBlog(blogItem);
    }

    private UserDetails setAuthentication() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(ADMIN_USER, ADMIN_PASSWORD);
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }
}