package org.metropolis.blogger.service;

import org.junit.Assert;
import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.metropolis.blogger.helper.AuthHeaderBuilder;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.annotation.Resource;

/**
 * Created by metropolis on 2/11/2017.
 */
public class AuthServiceTest extends BloggerContextBootStrap {
    @Resource
    private AuthService authService;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";

    private static final String NEW_USER_USER = "new.account@email.com";
    private static final String NEW_USER_PASSWORD = "password";

    @Resource
    private AuthHeaderBuilder authHeaderBuilder;

    @Test
    public void authenticateAndVerifyUser() {
        String credentials = authHeaderBuilder.generateAuthHeader(ADMIN_USER,ADMIN_PASSWORD);
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.addHeader("Authorization", credentials);
        Assert.assertTrue(authService.login(mockHttpServletRequest));
        Assert.assertTrue(authService.getAuthStatus());
        Assert.assertEquals(authService.getCurrentUserName(), "role.admin@email.com");
        Assert.assertEquals(authService.logout(), "redirect:/#/login");
    }

    @Test
    public void createNewAccount() {
        Assert.assertTrue(authService.createNewAccount(NEW_USER_USER, NEW_USER_PASSWORD, "New", "Account"));
    }
}
