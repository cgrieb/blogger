package org.metropolis.blogger.service;

import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

/**
 * Created by metropolis on 2/12/2017.
 */
public class CacheServiceTest extends BloggerContextBootStrap {
    @Autowired
    private CacheService cacheService;

    @Test
    public void confirmCacheClearing() {
        String ret = cacheService.clearCache();
        Assert.notNull(ret);
    }
}
