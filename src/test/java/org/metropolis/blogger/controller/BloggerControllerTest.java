package org.metropolis.blogger.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.metropolis.blogger.entity.BlogComment;
import org.metropolis.blogger.entity.BlogData;
import org.metropolis.blogger.entity.BlogItem;
import org.metropolis.blogger.repository.BlogDataRepository;
import org.metropolis.blogger.repository.BlogItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.nio.charset.Charset;
import java.util.Date;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by metropolis on 2/5/2017.
 */
public class BloggerControllerTest extends BloggerContextBootStrap {
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Resource
    private BlogItemRepository blogItemRepository;

    @Resource
    private BlogDataRepository blogDataRepository;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_ROLE = "admin";

    private static BlogItem newBlogItem;
    private static BlogComment blogComment;
    private static ObjectMapper objectMapper;
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));


    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);

        newBlogItem = new BlogItem();
        newBlogItem.setName("controller-blog");
        newBlogItem.setTitle("Controller Blog");
        newBlogItem.setDescription("This is a test blog from the controller blog");
        newBlogItem.setOwner("role.admin@email.com");
        newBlogItem.setEnabled(false);
        newBlogItem.setSecured(false);
        newBlogItem.setDeleted(true);
        newBlogItem.setCreated(new Date().getTime());

        blogComment = new BlogComment();
        blogComment.setBlogName("foobar");
        blogComment.setComment("This is a comment for foobar");
        blogComment.setCommentOwner("larry.person@yahoo.com");
        blogComment.setBlogPosting(1486224744);
        blogComment.setCommentPosted(1486224744);

        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getAuthStatus() throws Exception {
        mockMvc.perform(get("/blogger/getAllSitesForUser").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void createNewBlog() throws Exception {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(newBlogItem);

        mockMvc.perform(post("/blogger/createNewBlog").contentType(APPLICATION_JSON_UTF8).content(request).with(user(ADMIN_USER)
                .password(ADMIN_PASSWORD).roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void saveExistingBlog() throws Exception {
        BlogItem blogItem = blogItemRepository.findBlogFromName("foobar");
        blogItem.setDescription("Yep we're still foobar");
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(blogItem);

        mockMvc.perform(post("/blogger/saveExistingBlog").contentType(APPLICATION_JSON_UTF8).content(request).with(user(ADMIN_USER)
                .password(ADMIN_PASSWORD).roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void getSiteTotal() throws Exception {
        mockMvc.perform(get("/blogger/getSiteTotal").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void getDataFromFirstBlock() throws Exception {
        mockMvc.perform(get("/blogger/getDataFromFirstBlock/foobar").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void saveDataForBLog() throws Exception {
        BlogData blogData = blogDataRepository.findOne(6);
        blogData.setData("This is still the first data block");
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(blogData);

        mockMvc.perform(post("/blogger/saveDataForBLog").contentType(APPLICATION_JSON_UTF8).content(request).with(user(ADMIN_USER)
                .password(ADMIN_PASSWORD).roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void createNewPosting() throws Exception {
        mockMvc.perform(get("/blogger/createNewPosting").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)).param("blogName", "foobar"))
                .andExpect(status().isOk());
    }

    @Test
    public void addBlogComment() throws Exception {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(blogComment);

        mockMvc.perform(post("/blogger/addBlogComment").contentType(APPLICATION_JSON_UTF8).content(request)
                .with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void deleteComment() throws Exception {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(blogComment);

        mockMvc.perform(post("/blogger/deleteComment").contentType(APPLICATION_JSON_UTF8).content(request)
                .with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void search() throws Exception {
        mockMvc.perform(get("/blogger/search").param("search", "foobar").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void deactivateBlog() throws Exception {
        mockMvc.perform(post("/blogger/deactivateBlog").param("blogName", "foobar").with(user(ADMIN_USER).password(ADMIN_PASSWORD)
                .roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void getBlogCreatedDates() throws Exception {
        mockMvc.perform(get("/blogger/getBlogCreatedDates/foobar").with(user(ADMIN_USER).password(ADMIN_PASSWORD)
                .roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void getBlogAndOwnerInfo() throws Exception {
        mockMvc.perform(get("/blogger/getBlogAndOwnerInfo/foobar").with(user(ADMIN_USER).password(ADMIN_PASSWORD)
                .roles(ADMIN_ROLE))).andExpect(status().isOk());
    }

    @Test
    public void getDataAndComments() throws Exception {
        mockMvc.perform(get("/blogger/getDataAndComments/1486356655193").with(user(ADMIN_USER).password(ADMIN_PASSWORD)
                .roles(ADMIN_ROLE))).andExpect(status().isOk());
    }
}
