package org.metropolis.blogger.controller;

import org.junit.Before;
import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.metropolis.blogger.helper.AuthHeaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * Created by metropolis on 2/5/2017.
 */
public class AuthControllerTest extends BloggerContextBootStrap {
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_ROLE = "admin";

    @Resource
    private AuthHeaderBuilder authHeaderBuilder;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getAuthStatus() throws Exception {
        mockMvc.perform(get("/auth/getAuthStatus").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void login() throws Exception {
        String credentials = authHeaderBuilder.generateAuthHeader(ADMIN_USER, ADMIN_PASSWORD);
        mockMvc.perform(get("/auth/login").header("Authorization", credentials))
                .andExpect(status().isOk());
    }

    @Test
    public void logout() throws Exception {
        mockMvc.perform(get("/auth/logout").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/#/login"));
    }

    @Test
    public void create() throws Exception {
        mockMvc.perform(get("/auth/create").param("firstName", "Test").param("lastName", "User")
                .param("loginId", "test.user@email.com").param("password", "password")).andExpect(status().isOk());
    }

    @Test
    public void getUserFromUser() throws Exception {
            mockMvc.perform(get("/auth/getUserFromUser").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

}
