package org.metropolis.blogger.controller;

import org.junit.Before;
import org.junit.Test;
import org.metropolis.blogger.BloggerContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by metropolis on 2/5/2017.
 */
public class CacheControllerTest extends BloggerContextBootStrap {
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    private static final String USERNAME = "role.admin@email.com";
    private static final String PASSWORD = "password";
    private static final String ROLE = "admin";

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testCacheController() throws Exception{
        mockMvc.perform(get("/admin/clearCache").with(user(USERNAME).roles(ROLE)
                .password(PASSWORD))).andExpect(status().isOk());
    }
}
