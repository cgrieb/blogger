package org.metropolis.blogger;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.metropolis.blogger.controller.*;
import org.metropolis.blogger.service.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by metropolis on 2/11/2017.
 */
@RunWith(Suite.class)
//@Suite.SuiteClasses({
//        AuthControllerTest.class,
//        BloggerControllerTest.class,
//        CacheControllerTest.class,
//        IndexControllerTest.class,
//        AuthServiceTest.class,
//        BloggerServiceTest.class,
//        CacheControllerTest.class,
//        PasswordEncodeServiceTest.class,
//        CacheConfigTest.class
//})
public class RunAllTests extends BloggerContextBootStrap {
}
