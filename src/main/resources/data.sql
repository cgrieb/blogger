-- create table blogger_user_account(
--   username varchar(45) not null primary key,
--   password varchar(1024) not null,
--   first_name varchar(45) not null,
--   last_name varchar(45) not null,
--   enabled bit not null,
--   role varchar(45) not null
-- );
--
--
-- create table blog(
--   name varchar(45) not null primary key,
--   owner varchar(45) not null,
--   enabled bit not null,
--   secured bit not null,
--   deleted bit not null,
--   title varchar(45) not null,
--   description varchar(100) not null,
--   created BIGINT not null
-- );
--
-- create table blog_comments(
--   comment_id int not null auto_increment primary key,
--   blog_name varchar(45) not null,
--   comment_owner varchar(45) not null,
--   comment_posted bigint not null,
--   blog_posting bigint not null,
--   comment varchar(400) not null
-- );
--

drop table blog_data;

create table blog_data(
  data_id int not null auto_increment primary key,
  blog_name varchar(45) not null,
  data varchar(4000),
  enabled bit not null,
  created BIGINT not null,
  order_id int
);
CREATE SEQUENCE blog_data_seq START WITH 1000 INCREMENT BY 1 CACHE 10;
------------------------------------------------------------------------------------------------
insert into blogger_user_account (username, password,first_name,last_name,enabled,role) values(
  'role.admin@email.com',
  'password',
  'Admin',
  'User',
  1,
  'admin'
);
-- -------------------------------------------------------------------------------------------------
-- insert into blog (name, owner, enabled, secured, deleted, title, description, created) values (
--   'foobar',
--   'role.admin@email.com',
--   1,
--   0,
--   0,
--   'FooBar',
--   'All about FooBar',
--   1486269477918
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   6,
--   'foobar',
--   'This is the first data block',
--   1,
--   1486873833174,
--   1
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   7,
--   'foobar',
--   'This is the second data block',
--   1,
--   1486873833174,
--   2
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   8,
--   'foobar',
--   'This is the third data block',
--   1,
--   1486269477918,
--   3
-- );
-- -------------------------------------------------------------------------------------------------
-- insert into blog (name, owner, enabled, secured, deleted, title, description, created) values (
--   'this-that',
--   'role.admin@email.com',
--   1,
--   0,
--   0,
--   'This that',
--   'A blog about this and that',
--   1486356655193
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   9,
--   'this-that',
--   'This is the first data block',
--   1,
--   1486269477957,
--   1
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   10,
--   'this-that',
--   'This is the second data block',
--   1,
--   1486269477957,
--   2
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   11,
--   'this-that',
--   'This is the third data block',
--   1,
--   1486269477957,
--   3
-- );
-- --------------------------------------------------------------------------------------
-- insert into blog (name, owner, enabled, secured, deleted, title, description, created) values (
--   'hamminator',
--   'role.admin@email.com',
--   1,
--   0,
--   0,
--   'Other Stuff',
--   'A blog about Stuff and Things',
--   1486151435731
-- );

-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   13,
--   'hamminator',
--   'This is the first data block',
--   1,
--   1486151436249,
--   1
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   14,
--   'hamminator',
--   'This is the second data block',
--   1,
--   1486151436249,
--   2
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   15,
--   'hamminator',
--   'This is the third data block',
--   1,
--   1486151436249,
--   3
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   16,
--   'hamminator',
--   'This is the first data block',
--   1,
--   1486224744055,
--   1
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   17,
--   'hamminator',
--   'This is the second data block',
--   1,
--   1486224744055,
--   2
-- );
--
-- insert into blog_data (data_id, blog_name, data, enabled, created, order_id ) values (
--   18,
--   'hamminator',
--   'This is the third data block',
--   1,
--   1486224744055,
--   3
-- );
-- --------------------------------------------------------------------------------------
-- insert into blog_comments (comment_id, blog_name, comment_owner, comment_posted, blog_posting, comment) values (
--   1,
--   'hamminator',
--   'some.person@gmail.com',
--   1486262513657,
--   1486224744055,
--   'This is the first comment'
-- );
--
-- insert into blog_comments (comment_id, blog_name, comment_owner, comment_posted, blog_posting, comment) values (
--   2,
--   'hamminator',
--   'someother.person@gmail.com',
--   1486262513659,
--   1486224744055,
--   'This is the second comment'
-- );
