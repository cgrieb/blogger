package org.metropolis.blogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by metropolis on 3/8/2018.
 */
@SpringBootApplication
@EnableJpaRepositories
@EnableCaching
@EnableTransactionManagement
@EnableWebMvc
public class BloggerApp {
    public static void main(String[] args) {
        SpringApplication.run(BloggerApp.class, args);
    }
}
