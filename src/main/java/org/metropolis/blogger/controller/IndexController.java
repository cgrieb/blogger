package org.metropolis.blogger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by metropolis on 2/2/2017.
 */
@Controller
@RequestMapping(value = "/")
public class IndexController {
    /**
     * index.html - primary site page.
     * @return "index."
     */
    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String index() {
        return "index";
    }
}
