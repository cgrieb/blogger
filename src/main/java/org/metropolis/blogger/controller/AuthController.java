package org.metropolis.blogger.controller;

import org.metropolis.blogger.entity.BloggerUserAccount;
import org.metropolis.blogger.service.AuthService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 5/23/16.
 */
@Controller
@RequestMapping(value = "/auth")
public class AuthController {

    @Resource
    private AuthService authService;

    /**
     * getAuthStatus - is the user authenticated or not?
     * @return is authenticated/not authenticated, true/false.
     */
    @RequestMapping(value = "/getAuthStatus", method = RequestMethod.GET)
    public @ResponseBody boolean getAuthStatus() {
        return authService.getAuthStatus();
    }

    /**
     * Secure user login - extracts user information via authentication headers.
     * @param httpServletRequest - HttpServletRequest.
     * @return successfully login/not successful login.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public @ResponseBody boolean login(HttpServletRequest httpServletRequest) {
        return authService.login(httpServletRequest);
    }

    /**
     * Logs a user out with Spring Security.
     * @return - redirect:/login page.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        return authService.logout();
    }

    /**
     * Create a new account.
     * @param firstName - first name.
     * @param lastName - last name.
     * @param loginId - login ID (e.g, email).
     * @param password - password.
     * @return true/false, successfully created/already exists.
     */
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public @ResponseBody boolean createNewAccount(@RequestParam(value = "firstName", required = true) String firstName, @RequestParam(value = "lastName", required = true) String lastName,
                                 @RequestParam(value = "loginId", required = true) String loginId,@RequestParam(value = "password", required = true) String password) {
        return authService.createNewAccount(password,loginId,firstName,lastName);
    }

    /**
     * Get a BloggerUserAccount from currently logged in user.
     * @return BloggerUserAccount object of currently logged in user.
     */
    @RequestMapping(value = "/getUserFromUser", method = RequestMethod.GET)
    public @ResponseBody BloggerUserAccount getUser() {
        return authService.getUserFromUser();
    }
 }
