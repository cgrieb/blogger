package org.metropolis.blogger.controller;

import org.metropolis.blogger.entity.BlogComment;
import org.metropolis.blogger.entity.BlogCreated;
import org.metropolis.blogger.entity.BlogData;
import org.metropolis.blogger.entity.BlogItem;
import org.metropolis.blogger.service.BloggerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/blogger")
public class BloggerController {
    @Resource
    private BloggerService bloggerService;

    /**
     * Get all sites for a specific, logged in user.
     * @return A list of sites owned by a user.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/getAllSitesForUser")
    public @ResponseBody List<BlogItem> getAllSitesForUser(@AuthenticationPrincipal UserDetails userDetails) {
        return bloggerService.getAllSitesForUser(userDetails);
    }

    /**
     * Create a new blog.
     * @param blogItem - BlogItem of the new blog we're creating.
     * @return - successfully created/not successfully created.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/createNewBlog", method = RequestMethod.POST)
    public @ResponseBody boolean createNewBlog(@RequestBody BlogItem blogItem, @AuthenticationPrincipal UserDetails userDetails) {
        return bloggerService.createNewBlog(blogItem,userDetails);
    }

    /**
     * Save an existing blog.
     * @param blogItem - zi blog we're updating.
     * @return - successfully updated/not successfully updated.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/saveExistingBlog", method = RequestMethod.POST)
    public @ResponseBody boolean saveExistingBlog(@RequestBody BlogItem blogItem) {
        bloggerService.saveExistingBlog(blogItem);
        return true;
    }

    /**
     * Get the total number of sites owned by a specific user.
     * @return An integer in the rage of 0 - 5, where 5 is the maximum number of blogs allowed.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/getSiteTotal", method = RequestMethod.GET)
    public @ResponseBody int getSiteTotal(@AuthenticationPrincipal UserDetails userDetails) {
        return bloggerService.getSiteTotal(userDetails);
    }

    /**
     * Get a BlogData object from the first block of data for a posting.
     * @param blogName - name of blog.
     * @return A single BlogData object.
     */
    @RequestMapping(value = "/getDataFromFirstBlock/{blogName}", method = RequestMethod.GET)
    public @ResponseBody BlogData getDataFromFirstBlock(@PathVariable(value = "blogName") String blogName) {
        return bloggerService.getDataByFirstDataBlock(blogName);
    }

    /**
     * Save exiting blog content.
     * @param data - BlogData object.
     * @return blog data saved/blog data not saved.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/saveDataForBLog",method = RequestMethod.POST)
    public @ResponseBody boolean saveBlogData(@RequestBody BlogData data) {
        bloggerService.saveBlogData(data);
        return true;
    }

    /**
     * create a new blog posting.
     * @param blogName - this is the name of the blog we're creating a new posting for.
     * @return - datetime in Long type of when the posting was created.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/createNewPosting/{blogName}", method = RequestMethod.GET)
    public @ResponseBody Long createNewPosting(@PathVariable(value = "blogName") String blogName, @AuthenticationPrincipal UserDetails userDetails ) {
        return bloggerService.createNewPosting(blogName,userDetails);
    }

    /**
     * Add a new individual blog comment.
     * @param blogComment - BlogComment object.
     * @return BlogComment added/BlogComment not added.
     */
    @RequestMapping(value = "/addBlogComment", method = RequestMethod.POST)
    public @ResponseBody boolean addBlogComment(@RequestBody BlogComment blogComment) {
        bloggerService.addBlogComment(blogComment);
        return true;
    }

    /**
     * Refresh blog comments when we're deleting a specific blog posting - this is specific for a single blog.
     * @param blogComment - BlogComment we're removing.
     * @return - a listing of updated comments.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
    public @ResponseBody List<BlogComment> deleteAndGetComments(@RequestBody BlogComment blogComment) {
        bloggerService.deleteComment(blogComment);
        return bloggerService.getCommentsForBlog(blogComment.getBlogName(), blogComment.getBlogPosting());
    }

    /**
     * Entry point for BLogItem searches, specifically title, name, or description.
     * @param search - search query.
     * @return List of BlogItems.
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public @ResponseBody List<BlogItem> search(@RequestParam(value = "search", required = true) String search) {
        return bloggerService.search(search);
    }

    /**
     * Entry point for deactivating a blog (e.g, 'deleting' it...but we're not really deleting the blog, just
     * deactivating it - the data persists in the database).
     * @param blogName - name of the blog we're deleting.
     * @return true.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/deactivateBlog", method = RequestMethod.POST)
    public @ResponseBody boolean deactivateBlog(@RequestParam(value = "blogName", required = true) String blogName) {
        bloggerService.deactivateBlog(blogName);
        return true;
    }

    /**
     * Obtain a listing of dates when postings were created.
     * @param blogName - name of the blog we're extracting dates on.
     * @return A List of BlogCreated items.
     */
    @RequestMapping(value = "/getBlogCreatedDates/{blogName}", method = RequestMethod.GET)
    public @ResponseBody List<BlogCreated> getBlogCreatedDates(@PathVariable(value = "blogName") String blogName) {
        return bloggerService.getBlogCreatedDates(blogName);
    }

    /**
     * Get blog and user information.
     * @param blogName - name of the blog we're getting information for.
     * @return - A Map<List,Object> of blog/user information.
     */
    @RequestMapping(value = "/getBlogAndOwnerInfo/{blogName}", method = RequestMethod.GET)
    public @ResponseBody Map<String,Object> getBlogAndUserInfo(@PathVariable(value = "blogName") String blogName) {
        return bloggerService.getBlogAndUserInfo(blogName);
    }

    /**
     * Get blog data and comments associated with the blog posting.
     * @param blogName - name of the blog we're extracting data for.
     * @param startDate - posting date.
     * @return - A Map<String,List> of comments and data.
     */
    @RequestMapping(value = "/getDataAndComments/{blogName}/{startDate}", method = RequestMethod.GET)
    public @ResponseBody Map<String,List> getDataAndComments(@PathVariable(value = "blogName") String blogName, @PathVariable(value = "startDate") long startDate) {
        return bloggerService.getDataAndComments(blogName,startDate);
    }
}
