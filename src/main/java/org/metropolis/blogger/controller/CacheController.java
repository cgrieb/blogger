package org.metropolis.blogger.controller;

import org.metropolis.blogger.service.CacheService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by metropolis on 9/7/2016.
 */
@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasAnyRole('admin')")
public class CacheController {
    @Resource
    private CacheService cacheService;

    /**
     * Entry point for clearing out all system cache.
     * @return "Cache cleared on date"
     */
    @RequestMapping(value = "/clearCache", method = RequestMethod.GET)
    @ResponseBody String clearCache() {
        return cacheService.clearCache();
    }
}
