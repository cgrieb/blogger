package org.metropolis.blogger.service.impl;

import org.metropolis.blogger.entity.BloggerUserAccount;
import org.metropolis.blogger.repository.UserAccountRepository;
import org.metropolis.blogger.service.AuthService;
import org.metropolis.blogger.service.PasswordEncodeService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.logging.Logger;

/**
 * Created by cgrieb on 5/23/16.
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private PasswordEncodeService passwordEncodeService;

    @Resource
    private UserAccountRepository userAccountRepository;

    private static final Logger LOGGER = Logger.getLogger(AuthServiceImpl.class.getName());
    private static final String ANONYMOUS_USER = "anonymousUser";

    public boolean getAuthStatus() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !authentication.getPrincipal().equals(ANONYMOUS_USER);
    }

    public boolean login(HttpServletRequest httpServletRequest) {
        String[] userNameAndPassword = buildCredentials(httpServletRequest.getHeader("Authorization").substring(("Basic").length()).trim());
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userNameAndPassword[0].toLowerCase(),userNameAndPassword[1]);
        try {
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            if(authentication.isAuthenticated()) {
                return true;
            }
        } catch (final Throwable throwable) {
            LOGGER.warning("** failed login for user: " + userNameAndPassword[0] + " message is: " + throwable.getMessage());
        }

        return false;
    }

    public boolean createNewAccount(String password,String loginId,String firstName,String lastName) {
        BloggerUserAccount bloggerUserAccount = new BloggerUserAccount();
        bloggerUserAccount.setUsername(loginId.toLowerCase());

        bloggerUserAccount.setLastName(lastName);
        bloggerUserAccount.setFirstName(firstName);
        bloggerUserAccount.setEnabled(true);
        bloggerUserAccount.setRole("admin");
        bloggerUserAccount.setPassword(passwordEncodeService.passwordEncode(password));

        userAccountRepository.save(bloggerUserAccount);
        try {
            userAccountRepository.save(bloggerUserAccount);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public  String getCurrentUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public BloggerUserAccount getUserFromUsername(String userName) {
        BloggerUserAccount bloggerUserAccount = userAccountRepository.findOne(userName);
        if(bloggerUserAccount!=null) {
            if(userName.equals(this.getCurrentUserName())) {
                bloggerUserAccount.setAuthenticated(true);
            } else {
                bloggerUserAccount.setAuthenticated(false);
            }
        }

        return bloggerUserAccount;
    }

    public BloggerUserAccount getUserFromUser() {
        String userName = this.getCurrentUserName();
        if(!userName.equals(ANONYMOUS_USER)) {
            return this.getUserFromUsername(userName);
        } else {
            BloggerUserAccount anonymousUserAccount = new BloggerUserAccount();
            anonymousUserAccount.setUsername(ANONYMOUS_USER);
            anonymousUserAccount.setAuthenticated(false);
            anonymousUserAccount.setPassword("");
            anonymousUserAccount.setEnabled(true);
            anonymousUserAccount.setFirstName("Anonymous");
            anonymousUserAccount.setLastName("User");
            anonymousUserAccount.setRole("USER");
            return anonymousUserAccount;
        }
    }

    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return "redirect:/#/login";
    }

    private String[] buildCredentials(String base64Credentials) {
        String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
        return credentials.split(":",2);
    }
}
