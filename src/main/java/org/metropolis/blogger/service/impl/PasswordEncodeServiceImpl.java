package org.metropolis.blogger.service.impl;

import org.metropolis.blogger.service.PasswordEncodeService;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

/**
 * Created by cgrieb on 5/26/16.
 */
@Service
public class PasswordEncodeServiceImpl implements PasswordEncodeService {
    private static final Logger LOGGER = Logger.getLogger(PasswordEncodeServiceImpl.class.getName());

    @Override
    public String passwordEncode(String value) {
        StringBuffer hexBuffer = new StringBuffer();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(value.getBytes());
            byte bytes[] = digest.digest();

            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                buffer.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            hexBuffer = new StringBuffer();
            for (int i=0;i<bytes.length;i++) {
                String hex=Integer.toHexString(0xff & bytes[i]);
                if(hex.length()==1) hexBuffer.append('0');
                hexBuffer.append(hex);
            }
        } catch (NoSuchAlgorithmException e) {
            LOGGER.warning(e.getMessage());
        }
        return hexBuffer.toString();
    }
}
