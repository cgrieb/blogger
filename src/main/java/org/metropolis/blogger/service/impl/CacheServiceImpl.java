package org.metropolis.blogger.service.impl;

import org.metropolis.blogger.service.CacheService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by metropolis on 9/7/2016.
 */
@Service
public class CacheServiceImpl implements CacheService {
    private static final String BLOGGER_ITEM_CACHE = "bloggerItemCache";
    private static final String BLOGGER_DATA_CACHE = "bloggerDataCache";
    private static final String BLOGGER_CREATED_CACHE = "bloggerCreatedCache";
    private static final String BLOGGER_COMMENT_CACHE = "bloggerCommentCache";
    private static final String USER_ACCOUNT_CACHE = "userAccountCache";

    @Override
    @CacheEvict(value = {BLOGGER_COMMENT_CACHE,BLOGGER_ITEM_CACHE,BLOGGER_DATA_CACHE,BLOGGER_CREATED_CACHE,USER_ACCOUNT_CACHE}, allEntries = true)
    public String clearCache() {
        Date date = new Date();
        return "Cache cleared on " + date;
    }
}
