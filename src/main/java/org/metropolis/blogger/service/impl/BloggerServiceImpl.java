package org.metropolis.blogger.service.impl;

import org.metropolis.blogger.entity.*;
import org.metropolis.blogger.repository.*;
import org.metropolis.blogger.service.AuthService;
import org.metropolis.blogger.service.BloggerService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by cgrieb on 5/22/16.
 */
@Service
public class BloggerServiceImpl implements BloggerService {
    @Resource
    private BlogItemRepository blogItemRepository;

    @Resource
    private BlogCreatedRepository blogCreatedRepository;

    @Resource
    private BlogCommentRepository blogCommentRepository;

    @Resource
    private BlogDataRepository blogDataRepository;

    @Resource
    private AuthService authService;

    private static final String BLOGGER_ITEM_CACHE = "bloggerItemCache";
    private static final String BLOGGER_DATA_CACHE = "bloggerDataCache";
    private static final String BLOGGER_CREATED_CACHE = "bloggerCreatedCache";
    private static final String BLOGGER_COMMENT_CACHE = "bloggerCommentCache";

    private static final List<String> templateData = new ArrayList<>(Arrays.asList("First area of content goes here - currently, this section is empty...",
            "The second area of content goes here - currently, this section is empty...", "The third area of content goes here - currently, this section is empty..."));

    @Override
    public List<BlogItem> getAllSitesForUser(UserDetails userDetails) {
        return blogItemRepository.getAllSites(userDetails.getUsername());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = BLOGGER_ITEM_CACHE, allEntries = true),
            @CacheEvict(value = BLOGGER_DATA_CACHE, allEntries = true)
    })
    public boolean createNewBlog(BlogItem blogItem, UserDetails userDetails) {
        blogItem.setName(blogItem.getName().toLowerCase());

        BlogItem doesExist = blogItemRepository.findBlogFromName(blogItem.getName());

        if(doesExist==null) {
            blogItemRepository.save(blogItem);
            long created = new Date().getTime();
            IntStream.range(0,templateData.size()).forEach(index -> {
                BlogData blogData = this.createBlogData(blogItem.getName(),created,templateData.get(index),index + 1,true);
                blogDataRepository.save(blogData);
            });

            return true;
        }

        return false;
    }

    @Override
    @CacheEvict(value = BLOGGER_DATA_CACHE,allEntries = true)
    public Long createNewPosting(String blogName,UserDetails userDetails) {
        BlogItem blogItem = blogItemRepository.findBlogFromName(blogName);
        long created = new Date().getTime();

        if(!blogItem.getOwner().equals(userDetails.getUsername())) {
            return 0L;
        } else {
            IntStream.range(0,templateData.size()).forEach(index->{
                int orderId = index + 1;
                BlogData blogData = new BlogData();
                blogData.setBlogName(blogName);
                blogData.setData(templateData.get(index));
                blogData.setCreated(created);
                blogData.setEnabled(true);
                blogData.setOrderId(orderId);
                blogDataRepository.save(blogData);
            });
        }
        return created;
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = BLOGGER_ITEM_CACHE, key = "#a0.name + '.findBlogFromName'"),
            @CacheEvict(value = BLOGGER_ITEM_CACHE, key = "#a0.owner + '.getAllSites'")
    })
    public void saveExistingBlog(BlogItem blogItem) {
        blogItemRepository.save(blogItem);
    }

    @Override
    public BlogItem getBlog(String blogName) {
        return blogItemRepository.findOne(blogName);
    }

    @Override
    public int getSiteTotal(UserDetails userDetails) {
        return blogItemRepository.getActiveBlogTotal(userDetails.getUsername());
    }

    @Override
    public BlogData getDataByFirstDataBlock(String blogName) {
        List<BlogData> dataList = blogDataRepository.getFirstPosting(blogName);
        return blogDataRepository.getFirstPosting(blogName).get(0);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = BLOGGER_DATA_CACHE, key = "#a0.blogName + '.getFirstPosting'"),
            @CacheEvict(value = BLOGGER_DATA_CACHE, key = "#a0.blogName + '.' + #a0.created + '.getData'")
    })
    public void saveBlogData(BlogData data) {
        blogDataRepository.save(data);
    }

    @Override
    @CacheEvict(value = BLOGGER_COMMENT_CACHE, key = "#a0.blogName + '.comment.' + #a0.blogPosting")
    public void addBlogComment(BlogComment blogComment) {
        BlogItem blogItem = blogItemRepository.findBlogFromName(blogComment.getBlogName());
        if(blogItem!=null&&blogItem.getName().equals(blogComment.getBlogName())) {
            blogCommentRepository.save(blogComment);
        }
    }

    @Override
    public List<BlogComment> getCommentsForBlog(String blogName, Long blogPosting) {
        return blogCommentRepository.getCommentsForBlog(blogName,blogPosting);
    }

    @Override
    @CacheEvict(value = BLOGGER_COMMENT_CACHE, key = "#a0.blogName + '.comment.' + #a0.blogPosting")
    public void deleteComment(BlogComment blogComment) {
        blogCommentRepository.delete(blogComment);
    }

    @Override
    public List<BlogItem> search(String search) {
        return blogItemRepository.findFromDescriptionTitleName(search);
    }

    @Override
    @CacheEvict(value = BLOGGER_CREATED_CACHE, key = "#a0 + '.blogCreatedRepository'")
    public  List<BlogCreated> getBlogCreatedDates(String blogName) {
        return blogCreatedRepository.findAll(blogName);
    }

    @Override
    public Map<String,Object> getBlogAndUserInfo(String blogName) {
        Map<String,Object> blogAndUserInformation = new HashMap<String, Object>();
        BlogItem blogItem = blogItemRepository.findBlogFromName(blogName);
        blogAndUserInformation.put("blog",blogItem);
        blogAndUserInformation.put("user",authService.getUserFromUsername(blogItem.getOwner()));
        return blogAndUserInformation;
    }

    @Override
    public Map<String,List> getDataAndComments(String blogName, long startDate) {
        HashMap<String,List> dataAndComments = new HashMap<String, List>();
        dataAndComments.put("comments", blogCommentRepository.getCommentsForBlog(blogName,startDate));
        dataAndComments.put("data",blogDataRepository.getData(blogName,startDate));
        return dataAndComments;
    }

    @Override
    @CacheEvict(value = BLOGGER_ITEM_CACHE, allEntries = true)
    public  void deactivateBlog(String blogName) {
        BlogItem blogItem = blogItemRepository.findBlogFromName(blogName);
        blogItem.setDeleted(true);
        blogItemRepository.save(blogItem);
    }

    /**
     * This allows us to create our initial block of blog templateData.
     * @param blogName - name of blog.
     * @param created - when we created it (note: this is the same value for all templateData sets).
     * @param data - templateData string.
     * @return newly created blog object.
     */
    private BlogData createBlogData(String blogName,long created,String data,int orderId,boolean enabled) {
        BlogData blogData = new BlogData();
        blogData.setBlogName(blogName);
        blogData.setCreated(created);
        blogData.setData(data);
        blogData.setOrderId(orderId);
        blogData.setEnabled(enabled);
        return blogData;
    }
}