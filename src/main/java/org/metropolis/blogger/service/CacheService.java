package org.metropolis.blogger.service;

/**
 * Created by metropolis on 9/7/2016.
 */
public interface CacheService {
    /**
     * Clear all system cache.
     * @return - A string containing the date.
     */
    String clearCache();
}
