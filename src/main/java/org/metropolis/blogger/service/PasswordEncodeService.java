package org.metropolis.blogger.service;

/**
 * Created by cgrieb on 5/26/16.
 */
public interface PasswordEncodeService {
    /**
     * Encodes a password.
     * @param value - Value we're encoding.
     * @return encoded password.
     */
    String passwordEncode(String value);
}
