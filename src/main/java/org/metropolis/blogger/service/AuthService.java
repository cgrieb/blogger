package org.metropolis.blogger.service;

import org.metropolis.blogger.entity.BloggerUserAccount;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 5/23/16.
 */
public interface AuthService {
    /**
     * getAuthStatus - checks to see if a user is logged in.
     * @return true/false, logged in, not logged in.
     */
    boolean getAuthStatus();

    /**
     * Secure user login.
     * @param httpServletRequest - HttpServletRequest.
     * @return user successfully logged in / failed to successfully login.
     */
    boolean login(HttpServletRequest httpServletRequest);

    /**
     * Logout a user.
     * @return  a redirect to the login page.
     */
    String logout();

    /**
     * Create a new user account.
     * @param password - password.
     * @param loginId - loginId (email).
     * @param firstName - first name.
     * @param lastName last name.
     * @return successfully created/not successfully created.
     */
    boolean createNewAccount(String password,String loginId,String firstName,String lastName);

    /**
     * Get the currently authenticated account userName
     * @return userName of authenticated user.
     */
    String getCurrentUserName();

    /**
     * Get a user object.
     * @param userName - userName.
     * @return A blogger user account object.
     */
    BloggerUserAccount getUserFromUsername(String userName);

    /**
     * Get user from logged in user.
     * @return BloggerUserAccount
     */
    BloggerUserAccount getUserFromUser();
}
