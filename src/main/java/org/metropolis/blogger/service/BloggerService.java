package org.metropolis.blogger.service;

import org.metropolis.blogger.entity.BlogComment;
import org.metropolis.blogger.entity.BlogCreated;
import org.metropolis.blogger.entity.BlogData;
import org.metropolis.blogger.entity.BlogItem;
import org.springframework.security.core.userdetails.UserDetails;

import java.beans.Transient;
import java.util.List;
import java.util.Map;

/**
 * Created by cgrieb on 5/22/16.
 */
public interface BloggerService {
    /**
     * Repository interface for getting all sites for a user.
     * @return A List of BlogItems.
     */
    List<BlogItem> getAllSitesForUser(UserDetails userDetails);

    /**
     * Create a new blog
     * @param blogItem - BlogItem we're attempting to save.
     * @param userDetails - UserDetails object.
     * @return - blog was created successfully/not created successfully (already exists, etc).
     */
    boolean createNewBlog(BlogItem blogItem,UserDetails userDetails);

    /**
     * Repository interface for saving an exiting blog.
     * @param blogItem - A blog we're saving.
     */
    @Transient
    void saveExistingBlog(BlogItem blogItem);

    /**
     * Repository interface for getting a single BlogItem.
     * @param blogName - name of blog we're extracting.
     * @return A BlogItem.
     */
    BlogItem getBlog(String blogName);

    /**
     * Repository interface for getting the total number of blogs owned by a user.
     * @return total number of blogs currently owned by a user.
     */
    int getSiteTotal(UserDetails userDetails);

    /**
     * Repository interface for getting the first data entry associated with a given posting.
     * @param blogName - name of blog we're getting data for.
     * @return BlogData object.
     */
    BlogData getDataByFirstDataBlock(String blogName);

    /**
     * Repository interface for updating a BlogData entry.
     * @param data - BlogData to update.
     */
    @Transient
    void saveBlogData(BlogData data);

    /**
     * Creates a complete posting and all associated data.
     * @param blogName -  name of blog we're updating data for.
     * @return - date that the data was added.
     */
    Long createNewPosting(String blogName,UserDetails userDetails);

    /**
     * Repository interface for adding a new BlogComment.
     * @param blogComment - BlogComment we're adding.
     */
    void addBlogComment(BlogComment blogComment);

    /**
     * Repository interface for deleting and getting a BlogComment.
     * @param blogComment - BlogComment we're getting rid of.
     */
    @Transient
    void deleteComment(BlogComment blogComment);

    List<BlogComment> getCommentsForBlog(String blogName, Long blogPosting);

    /**
     * Repository interface for searching for blogs.
     * @param search - search string we're looking up.
     * @return A List of BlogItems.
     */
    List<BlogItem> search(String search);

    /**
     * Repository interface for getting all created dates for a given blog.
     * @param blogName - name of the blog we're getting data for.
     * @return A List of BlogCreated dates.
     */
    List<BlogCreated> getBlogCreatedDates(String blogName);

    /**
     * Repository interface for deactivating a blog.
     * @param blogName - name of blog we're deactivating.
     */
    @Transient
    void deactivateBlog(String blogName);

    /**
     * Extract blog data and owner information.
     * @param blogName - name of the blog we're getting data for.
     * @return - a Map<String,Object> with our blog and blog owner.
     */
    Map<String,Object> getBlogAndUserInfo(String blogName);

    /**
     * Extracts blog data and comments.
     * @param blogName - name of the blog we're getting data for.
     * @param startDate - posting data of the blog.
     * @return - Map<String,List> of comments and data.
     */
    Map<String,List> getDataAndComments(String blogName, long startDate);
}
