package org.metropolis.blogger.repository;

import org.metropolis.blogger.entity.BlogComment;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by cgrieb on 11/7/16.
 */
@Repository
public interface BlogCommentRepository extends JpaRepository<BlogComment,Long> {
    String BLOGGER_COMMENT_CACHE = "bloggerCommentCache";

    @Cacheable(value = BLOGGER_COMMENT_CACHE, key = "#a0 + '.comment.' + #a1")
    @Query("select c from BlogComment c where c.blogName = :blogName and c.blogPosting = :blogPosting order by c.commentPosted desc")
    List<BlogComment> getCommentsForBlog(@Param(value = "blogName") String blogName,@Param(value = "blogPosting") long blogPosting);
}
