package org.metropolis.blogger.repository;

import org.metropolis.blogger.entity.BlogItem;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 11/6/16.
 */
@Repository
public interface BlogItemRepository extends JpaRepository<BlogItem,String> {
    String BLOGGER_ITEM_CACHE = "bloggerItemCache";

    @Override
    @Query("select b from BlogItem b where b.name = :blogName")
    BlogItem findOne(@Param(value = "blogName") String blogName);

    @Cacheable(value = BLOGGER_ITEM_CACHE, key = "#a0 + '.getAllSites'")
    @Query("select b from BlogItem as b where b.owner = :username and b.deleted = false")
    List<BlogItem> getAllSites(@Param(value = "username") String username);

    @Query("select count(blog) from BlogItem blog where blog.owner =:username and blog.deleted = false")
    int getActiveBlogTotal(@Param(value = "username") String username);

    @Query("select b from BlogItem b where lower(b.name) like lower(:search) " + "or lower(b.description) like lower(:search) " +
            "or lower(b.title) like lower(:search) and b.enabled = true and b.deleted = false")
    List<BlogItem> findFromDescriptionTitleName(@Param(value = "search") String search);

    @Cacheable(value = BLOGGER_ITEM_CACHE, key = "#a0 + '.findBlogFromName'")
    @Query("select b from BlogItem  b where b.name =:name and b.deleted = false")
    BlogItem findBlogFromName(@Param(value = "name") String name);
}
