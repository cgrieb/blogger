package org.metropolis.blogger.repository;

import org.metropolis.blogger.entity.BlogData;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by cgrieb on 11/7/16.
 */
@Repository
public interface  BlogDataRepository extends JpaRepository<BlogData,Integer> {
    String BLOGGER_DATA_CACHE = "bloggerDataCache";

    @Cacheable(value = BLOGGER_DATA_CACHE, key = "#a0 + '.getFirstPosting'")
    @Query("select d from BlogData d where d.blogName = :blogName order by d.created")
    List<BlogData> getFirstPosting(@Param(value = "blogName") String blogName);

    @Cacheable(value = BLOGGER_DATA_CACHE, key = "#a0 + '.' + #a1 + '.getData'")
    @Query("select d from BlogData  d where d.blogName = :blogName and d.created = :startDate order by d.orderId")
    List<BlogData> getData(@Param(value = "blogName") String blogName, @Param(value = "startDate") long startDate);
}
