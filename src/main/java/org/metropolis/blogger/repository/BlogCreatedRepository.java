package org.metropolis.blogger.repository;

import org.metropolis.blogger.entity.BlogCreated;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 11/6/16.
 */
@Repository
public interface BlogCreatedRepository extends JpaRepository<BlogCreated,String> {
    String BLOGGER_CREATED_CACHE = "bloggerCreatedCache";

    @Cacheable(value = BLOGGER_CREATED_CACHE, key = "#a0 + '.blogCreatedRepository'")
    @Query("select distinct c.created from BlogCreated c where c.blogName = :blogName order by c.created desc")
    List<BlogCreated> findAll(@Param(value = "blogName") String blogName);
}
