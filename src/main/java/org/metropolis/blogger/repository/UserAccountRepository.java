package org.metropolis.blogger.repository;

import org.metropolis.blogger.entity.BloggerUserAccount;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by metropolis on 5/25/2016.
 */
@Repository
public interface UserAccountRepository extends JpaRepository<BloggerUserAccount,String> {
    String USER_ACCOUNT_CACHE = "userAccountCache";

    @Override
    @Cacheable(value = USER_ACCOUNT_CACHE, key = "#a0" + " + '.fromUserName'")
    @Query("select user from BloggerUserAccount user where user.username =:userName")
    BloggerUserAccount findOne(@Param(value = "userName") String userName);
}
