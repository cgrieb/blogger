package org.metropolis.blogger.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by metropolis on 5/29/2016.
 */
@Entity
@Table(name = "blog")
public class BlogItem implements Serializable {
    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "owner")
    private String owner;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "secured")
    private boolean secured;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "created")
    private Long created;

    @Column(name = "deleted")
    private boolean deleted;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean getSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public boolean isSecured() {
        return secured;
    }

    public boolean isDeleted() {return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
