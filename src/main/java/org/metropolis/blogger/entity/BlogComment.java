package org.metropolis.blogger.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by cgrieb on 6/15/16.
 */
@Entity
@Table(name = "blog_comments")
public class BlogComment implements Serializable {
    @Id
    @Column(name = "comment_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int commentId;

    @Column(name = "blog_name")
    private String blogName;

    @Column(name = "comment_owner")
    private String commentOwner;

    @Column(name = "comment_posted")
    private long commentPosted;

    @Column(name = "blog_posting")
    private long blogPosting;

    @Column(name = "comment")
    private String comment;

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentOwner() {
        return commentOwner;
    }

    public void setCommentOwner(String commentOwner) {
        this.commentOwner = commentOwner;
    }

    public long getCommentPosted() {
        return commentPosted;
    }

    public void setCommentPosted(long commentPosted) {
        this.commentPosted = commentPosted;
    }

    public long getBlogPosting() {
        return blogPosting;
    }

    public void setBlogPosting(long bloPosting) {
        this.blogPosting = bloPosting;
    }
}
