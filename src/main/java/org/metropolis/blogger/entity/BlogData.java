package org.metropolis.blogger.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by cgrieb on 6/16/16.
 */
@Entity
@Table(name = "blog_data")
public class BlogData implements Serializable {
    @Id
    @Column(name = "data_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int dataId;

    @Column(name = "blog_name")
    private String blogName;

    @Column(name = "data")
    private String data;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "created")
    private long created;

    @Column(name = "order_id")
    private int orderId;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public int getDataId() {
        return dataId;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public int getOrderId() { return orderId;}

    public void setOrderId(int orderId) { this.orderId = orderId;}
}
