package org.metropolis.blogger.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by cgrieb on 8/7/16.
 */
@Entity
@Table(name = "blog_data")
public class BlogCreated implements Serializable {
    @Id
    @Column(name = "blog_name")
    private String blogName;

    @Column(name = "created")
    private long created;

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }
}
