/**
 * Created by cgrieb on 5/23/16.
 */
angular.module('BloggerApp').service('AuthService', ['$q','$http',function($q,$http){
    var userDataKey = "userData";

    this.getUserFromUser = function() {
        var deferred = $q.defer();
        $http({
            url:'/auth/getUserFromUser',
            method:'GET',
            timeout:2000
        }).success(function(response){
           deferred.resolve(response);
        }).error(function(){
            deferred.resolve(null);
        });
        return deferred.promise;
    };

    this.getAuthStatus = function() {
        var deferred = $q.defer();
        $http({
            url:'/auth/getAuthStatus',
            method:'GET',
            timeout:2000
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.login = function(authHeader) {
        var deferred = $q.defer();
        $http({
            url:'/auth/login',
            method:'GET',
            timeout:2000,
            headers: {'Authorization':authHeader}
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.setUserData = function(user) {
        sessionStorage.removeItem(userDataKey);
        sessionStorage.setItem(userDataKey, JSON.stringify(user));
    };

    this.getUserData = function() {
        return JSON.parse(sessionStorage.getItem(userDataKey));
    };
}]);