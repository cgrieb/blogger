/**
 * Created by cgrieb on 9/18/16.
 */
angular.module('BloggerApp').controller('404Controller', ['$scope','AuthService',function($scope,AuthService){
    $scope.authenticated = false;
    AuthService.getAuthStatus().then(function(response){
        $scope.authenticated = response
    })
}]);