/**
 * Created by cgrieb on 5/25/16.
 */
angular.module('BloggerApp').controller('LandingController', ['$scope','$uibModal','BloggerService','AuthService','$window','$route',function($scope,$uibModal,BloggerService,AuthService,$window,$route){
    // Blogs owned by the user.
    $scope.blogs = [];

    // A listing of initial postings.
    $scope.firstPostings = [];

    // Blog total.
    $scope.blogTotal = 0;

    // Maximum blogs allowed per-user.
    $scope.maxBlogsAllowed = 5;
    $scope.loaded = false;
    $scope.blogDeleted = false;
    $scope.blogDeletedName = "";

    initLandingController();

    $scope.openNewBlogModal = function () {
        $uibModal.open({
            templateUrl: '/static/angular/public/view/modal/create-new-blog.html',
            controller: 'CreateBlogController'
        });
    };

    $scope.openEditBlogModal = function(blog) {
        BloggerService.setBlog(blog);
        $uibModal.open({
            templateUrl: '/static/angular/public/view/modal/edit-existing-blog.html',
            controller: 'EditBlogController'
        });
    };

    $scope.linkBuilder =function(blogName) {
        for(var i = 0, j = $scope.firstPostings.length; i < j; i++) {
            var posting = $scope.firstPostings[i];
            if(posting.blogName === blogName) {
                return "/#/blog/Site/" + blogName + "/" + posting.created;
            }
        }
    };

    $scope.deactivateBlog = function(blogName) {
        $scope.blogDeletedName = blogName;
        BloggerService.deactivateBlog(blogName).then(function(response){
            if(response) {
                BloggerService.getAllSitesForUser().then(function(response){
                    $scope.blogs = response;
                    $scope.blogDeleted = true;
                });
            }
        });
    };

    function initLandingController() {
        AuthService.getAuthStatus().then(function(response){
           if(!response) {
               $window.location = "/#/login";
           } else {
               BloggerService.getBlogTotal().then(function(response){
                   $scope.blogTotal = response;
               });

               BloggerService.getAllSitesForUser().then(function(response){
                   $scope.blogs = response;
                   $scope.loaded = true;
                   for(var i = 0, j = $scope.blogs.length; i < j; i++) {
                       var blog = $scope.blogs[i];
                       BloggerService.getFirstPosting(blog.name).then(function(response){
                           $scope.firstPostings.push(response);
                       });
                   }
               });
           }
        });
    }
}]);