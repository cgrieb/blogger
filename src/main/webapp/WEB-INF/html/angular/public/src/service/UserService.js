/**
 * Created by metropolis on 5/27/2016.
 */
angular.module('BloggerApp').service('UserService', ['$q','$http',function($q,$http) {
    this.createNewAccount = function(userId,firstName,lastName,password) {
        var deferred = $q.defer();
        $http({
            url:'/auth/create',
            method:'GET',
            params:{firstName:firstName,lastName:lastName,loginId:userId,password:password}
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };
}]);