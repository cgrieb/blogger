/**
 * Created by metropolis on 5/29/2016.
 */
angular.module('BloggerApp').service('BloggerService',['$q','$http',function($q,$http){
    // Blog specific object.
    var blog = {};

    // Actual blog data.
    var data = [];

    // For when we do stuff.
    var viewingNewPost = false;

    // For search results.
    var searchResults = [];

    // Search string value.
    var searchItem = "";

    var blogCreatedKey = "blogCreatedKey";

    var TIMEOUT = 20000;

    this.getBlogAndOwnerInfo = function(blogName) {
        var deferred = $q.defer();
        $http({
            url: '/blogger/getBlogAndOwnerInfo/' + blogName,
            method: 'GET',
            timeout: TIMEOUT
        }).success(function (response) {
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getDataAndComments = function(blogName,postingDate) {
        var deferred = $q.defer();
         $http({
             url:'/blogger/getDataAndComments/' + blogName + "/" + postingDate,
             timeout:TIMEOUT
         }).success(function(response){
             deferred.resolve(response);
         });
        return deferred.promise;
    };

    this.getBlogCreatedDates = function(blogName) {
        var deferred = $q.defer();
        $http({
            url: '/blogger/getBlogCreatedDates/' + blogName,
            method: 'GET',
            timeout: TIMEOUT
        }).success(function (response) {
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getBlogCreatedDates = function(url) {
        var deferred = $q.defer();
        $http({
            url: url,
            method: 'GET',
            timeout: TIMEOUT
        }).success(function (response) {
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getFirstPosting = function(blogName) {
        var deferred = $q.defer();
        $http({
            url:'/blogger/getDataFromFirstBlock/' + blogName,
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.deactivateBlog = function(blog) {
        var deferred = $q.defer();
        $http({
            url:'/blogger/deactivateBlog',
            method:'POST',
            params:{blogName:blog},
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.normalizedDataEntry = function(data,url,method) {
        var deferred = $q.defer();
        $http({
            url:url,
            method:method,
            data:JSON.stringify(data),
            time:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.saveDataForBlog = function(data) {
        var deferred = $q.defer();
        $http({
            url:'/blogger/saveDataForBLog',
            method:'POST',
            data:JSON.stringify(data),
            time:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.saveExistingBlog = function(blog) {
        var deferred = $q.defer();
        $http({
            url:'/blogger/saveExistingBlog',
            method:'POST',
            data:JSON.stringify(blog),
            time:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.getAllSitesForUser = function() {
        var deferred = $q.defer();
        $http({
            url:'/blogger/getAllSitesForUser',
            method: 'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.createNewBlog = function(blog) {
        var deferred = $q.defer();
        $http({
            url:'/blogger/createNewBlog',
            method:'POST',
            data:JSON.stringify(blog),
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getBlogTotal = function() {
        var deferred = $q.defer();
        $http({
            url:'/blogger/getSiteTotal',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.saveNewPost = function(blogName) {
        var deferred = $q.defer();
        $http({
            url:'/blogger/createNewPosting/' + blogName,
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.executeSearch = function(search) {
        searchItem = search;
        var deferred = $q.defer();
        $http({
            url:'/blogger/search',
            method:'GET',
            timeout:TIMEOUT,
            params:{search:search}
        }).success(function(resonse){
                deferred.resolve(resonse);
        });
        return deferred.promise;
    };

    this.setBlog = function(object) {
        blog = object;
    };

    this.getBlog = function() {
        return blog;
    };

    this.setData = function(object) {
        data = object;
    };

    this.getData = function() {
        return data;
    };

    this.setViewingNewPost = function(value) {
        viewingNewPost = value;
    };

    this.getViewingNewPost = function() {
        return viewingNewPost;
    };

    this.setSearchResults = function(results) {
        searchResults = results;
    };

    this.getSearchResults = function() {
        return searchResults;
    };

    this.getSearchString = function() {
        return searchItem;
    };
}]);