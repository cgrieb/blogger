/**
 * Created by cgrieb on 5/25/16.
 */
angular.module('BloggerApp').controller('LoginController', ['$scope','$window','$location','$route','AuthService','BloggerService','$interval','$document',function($scope,$window,$location,$route,AuthService,BloggerService,$interval,$document){
    $scope.username = '';
    $scope.password = '';

    $scope.loginSuccessful = true;
    $scope.authenticated = false;

    $scope.searchValue = '';

    $scope.user = {};

    var interval = 10;
    var maxInterval = 180;

    initLoginController();

    function initLoginController() {
        startAuthTracker();
        var url = $location.url();
        AuthService.getAuthStatus().then(function(authenticated){
            $scope.authenticated = authenticated;

            if($scope.authenticated) {
                AuthService.getUserFromUser().then(function(user){
                    if($scope.user !== null) {
                        $scope.user = user;
                        AuthService.setUserData($scope.user);
                    } else {
                        $location.path("/logout")
                    }
                });
            }

            if(url === "/login" && $scope.authenticated) {

                $location.path("/landing");
            } else {
                $location.path(url);
            }
        });
    }

    $scope.login = function() {
        var authHeader = 'Basic ' + window.btoa($scope.username + ':' + $scope.password);
        AuthService.login(authHeader).then(function(response){
            if(response) {
                $scope.authenticated = true;
                // Note: We're reloading here to refresh the index view.
                $window.location.reload();
            } else {
                $scope.loginSuccessful = false;
                resetCredentials();
            }
        })
    };

    $scope.executeSearch = function() {
        if($scope.searchValue.length >= 3) {
            BloggerService.executeSearch($scope.searchValue).then(function(response){
                var url = $location.url();
                BloggerService.setSearchResults(response);
                if(url === '/search') {
                    $route.reload()
                } else {
                    $location.path("/search");
                }
            });
        }

        resetSearchValue();
    };

    $scope.isAuthenticated = function() {
        return $scope.authenticated;
    };

    $scope.validateCredentials = function() {
        var matchResults =  $scope.username.search(/^[a-zA-Z0-9\.-].*@[a-zA-Z0-9].*\.[a-zA-Z0-9].*$/);
        return (matchResults > -1 && ($scope.password.length >= 6));
    };

    $scope.buildPostingLinks = function(created) {
        return "/#/blog/Site/" + $scope.blogName + "/" + created;
    };

    function resetCredentials() {
        $scope.password = "";
        $scope.username = "";
    }

    function resetSearchValue() {
        $scope.searchValue = "";
    }

    function startAuthTracker() {
        $interval(intervalTracker,5000);
        $document.find('body').on("mousemove keydown mouseup", function(){
            interval = 0;
        });
    }

    function intervalTracker() {
        if($scope.authenticated) {
            if(interval>maxInterval) {
                console.log("** user session has timedout - logging out user now **");
                $window.location = "/auth/logout";
                interval = 0;
            } else {
                interval++;
            }
        }
    }
}]);