/**
 * Created by cgrieb on 6/19/16.
 */
angular.module('BloggerApp').controller('EditDataController', ['$scope','$uibModalInstance','BloggerService',function($scope,$uibModalInstance,BloggerService){
    $scope.data = BloggerService.getData();
    $scope.myDate = new Date().getTime();

    var originalData = $scope.data.data;
    var updateSuccessful = false;

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
    };

    $scope.save = function() {
        BloggerService.saveDataForBlog($scope.data).then(function(response){
            if(response) {
                updateSuccessful = true;
            }
            gracefullyDestroyEditor('data');
            $scope.cancel();
        });
    };

    $scope.cancel = function () {
        if(!updateSuccessful) {
            $scope.data.data = originalData;
        }
        gracefullyDestroyEditor('data');
        $uibModalInstance.dismiss('cancel');
    };

    $scope.canSaveData = function() {
       return $scope.data.data.length > 0;
    };

    $scope.toggleDataEnabled = function(value) {
        $scope.data.enabled = value;
    };

    var gracefullyDestroyEditor = function(id) {
        var instance = CKEDITOR.instances[id];
        if(instance) {
            instance.destroy(true);
        }
    };
}]);