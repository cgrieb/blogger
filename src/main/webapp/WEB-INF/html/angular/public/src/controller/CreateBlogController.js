/**
 * Created by metropolis on 5/29/2016.
 */
angular.module('BloggerApp').controller('CreateBlogController', ['$uibModalInstance','$scope','BloggerService','AuthService','$route',function($uibModalInstance,$scope,BloggerService,AuthService,$route){
    $scope.name = "";
    $scope.title = "";
    $scope.description = "";
    $scope.secured = false;
    $scope.exists = false;
    var blog = {};

    $scope.save = function () {
        var userData = AuthService.getUserData();
        var created = new Date().getTime();
        var owner = userData.username;

        blog = {
            description:$scope.description, name:$scope.name,title:$scope.title,
            secured:$scope.secured, enabled:true,created:created,owner:owner
        };

        BloggerService.createNewBlog(blog).then(function(response){
            if(response) {
                console.log("** blog " + $scope.name + " successfully created **");
                $uibModalInstance.close();
                $route.reload();

            } else {
                $scope.exists = true;
                $scope.name = "";
            }

            blog = {};
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.validateBlogInformation = function() {
        return ($scope.name.length>2&&$scope.name.search(/^[a-zA-Z]*-?[a-zA-Z]*-?[a-zA-Z]*$/)>-1
        &&$scope.title.length>2&&$scope.description.length>2);
    };

    $scope.setSecuredValue = function(value) {
        $scope.secured = value;
    }
}]);