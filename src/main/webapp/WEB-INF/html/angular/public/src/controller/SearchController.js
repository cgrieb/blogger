/**
 * Created by cgrieb on 7/20/16.
 */
angular.module('BloggerApp').controller('SearchController', ['BloggerService','$scope','$window',function(BloggerService,$scope,$window){
    $scope.firstPostings = [];

    initSearchController();
    function initSearchController() {
        // Get cached search results.
        $scope.blogs = BloggerService.getSearchResults();
        $scope.searchItem = BloggerService.getSearchString();
        $scope.total = $scope.blogs.length;

        // User is just hitting the page directly so redirect.
        if($scope.searchItem.length < 3) {
            $window.location = "/#/login";
        } else {
            for (var i = 0, j = $scope.blogs.length; i < j; i++) {
                var blog = $scope.blogs[i];
                BloggerService.getFirstPosting(blog.name).then(function(response){
                    $scope.firstPostings.push(response);
                });
            }
        }
    }

    $scope.searchLinkBuilder =function(blogName) {
        for(var i = 0, j = $scope.firstPostings.length; i < j; i++) {
            var posting = $scope.firstPostings[i];
            if(posting.blogName === blogName) {
                return "/#/blog/Site/" + blogName + "/" + posting.created;
            }
        }
    };
}]);