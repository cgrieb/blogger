/**
 * Created by metropolis on 5/25/2016.
 */
angular.module('BloggerApp').controller('CreateController', ['$scope','$location','UserService','AuthService',function($scope,$location,UserService,AuthService){
    $scope.userId = "";
    $scope.firstName = "";
    $scope.lastName = "";
    $scope.password = "";

    $scope.results = true;

    initCreateController();

    function initCreateController(){
        AuthService.getAuthStatus().then(function(response){
            if(response) {
                $location.path("/landing");
            }
        });
    }

    $scope.saveNewAccount = function() {
        UserService.createNewAccount($scope.userId,$scope.firstName,$scope.lastName,$scope.password).then(function(response){
            $scope.results = response;
            if(response) {
                console.log("** account created successfully **");
                $location.path('/login');
            } else {
                $scope.resetFields();
            }
        })
    };

    $scope.resetFields = function() {
        $scope.userId = "";
        $scope.firstName = "";
        $scope.lastName = "";
        $scope.password = "";
    };
}]);