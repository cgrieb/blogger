/**
 * Created by cgrieb on 5/19/16.
 */
angular.module('BloggerApp', ['ngRoute','ngResource','ngMessages','ngDraggable','ngFileUpload','angular-toArrayFilter','ui.bootstrap','ui.bootstrap.pagination','ckeditor'])
    .config(function($routeProvider){
    $routeProvider.when("/landing", {
        controller:"LandingController",
        templateUrl:"/static/angular/public/view/landing.html"
    }).when("/login", {
        controller: "LoginController",
        templateUrl:"/static/angular/public/view/login.html"
    }).when("/create", {
        controller: "CreateController",
        templateUrl:"/static/angular/public/view/create.html"
    }).when("/blog/Site/:name/:created", {
        controller: "BlogController",
        templateUrl: "/static/angular/public/view/blog.html"
    }).when("/search", {
        controller: "SearchController",
        templateUrl:"/static/angular/public/view/search.html"
    }).when("/404", {
        templateUrl:"/static/angular/public/view/404.html",
        controller:"404Controller"
    }).otherwise({
        redirectTo:"/login"
    });
});