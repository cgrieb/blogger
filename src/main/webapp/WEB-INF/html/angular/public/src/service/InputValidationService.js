/**
 * Created by cgrieb on 6/7/16.
 */
angular.module('BloggerApp').service('InputValidationService', [function(){
    this.validateInputData = function(value,regex) {
        return(value.search(regex) > -1);
    };
}]);