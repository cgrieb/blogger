/**
 * Created by metropolis on 6/11/2016.
 */
angular.module('BloggerApp').controller('BlogController', ['$route', '$scope','$routeParams','$window','BloggerService','AuthService','$uibModal','AlertService', function($route,$scope,$routeParams,$window,BloggerService,AuthService,$uibModal,AlertService) {
    $scope.infoAndDataLoaded = false;

    $scope.blog = {};
    $scope.blogOwner = {};

    $scope.dates = [];
    $scope.data = [];

    $scope.currentPosting = 0;
    $scope.nextPosting = 0;
    $scope.previousPosting = 0;

    $scope.blogName = $routeParams.name;
    $scope.created = parseInt($routeParams.created);

    $scope.comments = [];
    $scope.commentOwner = "";
    $scope.comment = "";
    $scope.hi = "";
    $scope.currentUser = {};

    $scope.viewingNewPost = false;
    $scope.alerts = [];

    var ROUTE_404_REDIRECT_URL = "/#/404";
    var ANONYMOUS_USER = "anonymousUser";

    initBlogController();

    // This watch allows to determination of actions after our required blog objects have been
    // initialized.
    $scope.$watch('[blog, data, currentUser]',
        function() {
           if($scope.data.length > 0 && Object.keys($scope.blog).length > 0 && Object.keys($scope.currentUser).length > 0) {
               if(!$scope.blog.enabled || $scope.blog.secured && $scope.currentUser.username === ANONYMOUS_USER||$scope.data.length === 0) {
                   $window.location = ROUTE_404_REDIRECT_URL;
               } else {
                   $scope.infoAndDataLoaded = true;
               }
           }
        },
        true
    );

    function initBlogController() {
        BloggerService.getBlogCreatedDates('/blogger/getBlogCreatedDates/' + $scope.blogName).then(function(dates){
            $scope.dates = dates;
            determinePostingOrder();
        });

        BloggerService.getBlogAndOwnerInfo($scope.blogName).then(function(response){
            $scope.blogOwner = response.user;
            $scope.blog = response.blog;

            if($scope.blogOwner.authenticated) {
                $scope.currentUser = $scope.blogOwner;
            } else {
                AuthService.getUserFromUser().then(function(response){
                    $scope.currentUser = response;
                });
            }
        });

        BloggerService.getDataAndComments($scope.blogName,$scope.created).then(function(response){
            $scope.comments = response.comments;
            $scope.data = response.data;
            $scope.infoAndDataLoaded = true;

            if($scope.data.length === 0) {
                $window.location = ROUTE_404_REDIRECT_URL;
            }
        });

        $scope.viewingNewPost = BloggerService.getViewingNewPost();
        if($scope.viewingNewPost) {
            $scope.alerts = [
                {type:AlertService.getType(), msg:AlertService.getMessage()}
            ];
        }
    }

    $scope.createNewPosting = function() {
        BloggerService.saveNewPost($scope.blogName).then(function(response){
            if(response) {
                BloggerService.setViewingNewPost(true);
                AlertService.setMessage("New post successfully created for blog: " + $scope.blog.title);
                AlertService.setType("success");
            }
            $window.location = "/#/blog/Site/" + $scope.blogName + "/" + response;
        });
    };

    $scope.dataEnabledOrOwner = function(enabled) {
        if((($scope.currentUser.username === $scope.blog.owner)) || enabled) {
            return true;
        }
        return false;
    };

    $scope.enableNoContent = function() {
        for(var i = 0; i < $scope.data.length; i++) {
            if($scope.data[i].enabled) {
                return false;
            }
        }

        return true;
    };

    $scope.openEditDataModal = function(data) {
        BloggerService.setData(data);
        $uibModal.open({
            templateUrl: '/static/angular/public/view/modal/edit-existing-data.html',
            controller: 'EditDataController',
            size:'lg'
        });
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index,1);
        BloggerService.setViewingNewPost(false)
    };

    $scope.saveBlogComment = function() {
        var now = new Date();
        var commentPosted = now.getTime();
        var blogComment = {blogName:$scope.blogName, commentOwner:$scope.commentOwner, comment:$scope.comment,
            blogPosting:$scope.created, commentPosted:commentPosted
        };

        BloggerService.normalizedDataEntry(blogComment,'/blogger/addBlogComment','POST').then(function(response) {
            if(response) {
                $scope.comments.unshift(blogComment);
            }
        });

        $scope.commentOwner = "";
        $scope.comment = "";
    };

    $scope.deleteComment = function(comment) {
        BloggerService.normalizedDataEntry(comment,'/blogger/deleteComment', 'POST').then(function(response){
            $scope.comments = response;
        });
    };

    $scope.validateCommentInformation = function() {
        return($scope.commentOwner.length > 4 && $scope.comment.length >=2);
    };

    function determinePostingOrder() {
        for(var i = 0, j = $scope.dates.length; i < j; i++) {
            if($scope.dates[i] === $scope.created) {
                if((i + 1) < j) {
                    $scope.nextPosting = $scope.dates[(i + 1)];
                }

                if((i - 1) > -1) {
                    $scope.previousPosting = $scope.dates[(i - 1)];
                }
            }
        }
    }
}]);