/**
 * Created by cgrieb on 6/3/16.
 */
angular.module('BloggerApp').controller('EditBlogController', ['$scope','$route','BloggerService','$uibModalInstance','InputValidationService',function($scope,$route,BloggerService,$uibModalInstance,InputValidationService){
    $scope.blog = BloggerService.getBlog();
    $scope.title = $scope.blog.title;

    var blogCopy = {};
    angular.copy($scope.blog,blogCopy);

    $scope.save = function() {
        BloggerService.saveExistingBlog($scope.blog).then(function(response){
            if(!response) {
                console.log("** unable to update blog: " + $scope.blog.name + " **");
            }
            $scope.cancel();
            $route.reload();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        angular.copy(blogCopy,$scope.blog);
    };

    $scope.validateBlogInformation = function() {
        if($scope.blog.title.length >= 2 && $scope.blog.description.length >= 2) {
            return true;
        }
        return false;
    };

    $scope.setSecuredBlog = function(value) {
        $scope.blog.secured = value;
    };

    $scope.setEnabledBlog = function(value) {
        $scope.blog.enabled = value;
    }
}]);