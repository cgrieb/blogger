/**
 * Created by metropolis on 9/4/2016.
 */
angular.module('BloggerApp').filter('trustAsHtml',["$sce", function($sce){
    return function(markup){
        return $sce.trustAsHtml(markup);
    }
}]);