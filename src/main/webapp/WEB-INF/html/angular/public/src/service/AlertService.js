/**
 * Created by cgrieb on 7/9/16.
 */
angular.module('BloggerApp').service('AlertService',[function(){
    /**
     * These are default values for our messages.
     */
    this.message = "This is an alert message";
    this.type = "success";

    this.setMessage = function(message) {
        this.message = message;
    };

    this.getMessage = function() {
        return this.message;
    };

    this.setType = function(type) {
        this.type = type;
    };

    this.getType = function() {
        return this.type;
    }
}]);